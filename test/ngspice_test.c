#include <stdbool.h>
#include <stdio.h>

#include "ngspice/sharedspice.h"
#include "sleep_ms.h"

int recieve_char(char *str, int id, void *p) {
  printf("recieved %s\n", str);
  return 0;
}

int recieve_stat(char *status, int id, void *p) {
  printf("status: %s\n", status);
  return 0;
}

int ngexit(int status, bool unload, bool exit, int id, void *p) {
  printf("exit: %d\n", status);
  return 0;
}

int recieve_data(vecvaluesall *data, int numstructs, int id, void *p) {
  // printf("data recieved: %f\n", data->vecsa[0]->creal);
  return 0;
}

int recieve_init_data(vecinfoall *data, int id, void *p) {
  printf("init data recieved from: %d\n", id);
  return 0;
}

int ngrunning(bool running, int id, void *p) {
  if (running) {
    printf("ng is running\n");
  } else {
    printf("ng is not running\n");
  }
  return 0;
}

int main(int argc, char **argv) {
  ngSpice_Init(&recieve_char, &recieve_stat, &ngexit, &recieve_data,
               &recieve_init_data, &ngrunning, (void *)NULL);

  char **circarray = (char **)malloc(sizeof(char *) * 7);
  circarray[0] = strdup("test array");
  circarray[1] = strdup("V1 1 0 1");
  circarray[2] = strdup("R1 1 2 1");
  circarray[3] = strdup("C1 2 0 1 ic=0");
  circarray[4] = strdup(".tran 10u 3 uic");
  circarray[5] = strdup(".end");
  circarray[6] = NULL;
  ngSpice_Circ(circarray);

  ngSpice_Command("run");
  sleep_ms(1000);
  ngSpice_Command("stop");

  return 0;
}
