#include <gtest/gtest.h>
#include "count_parameter.h"
using ::testing::FloatLE;

// Demonstrate some basic assertions.
TEST(HelloTest, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);
}

TEST(CountParameterTest, n00){
  EXPECT_PRED_FORMAT2(FloatLE, 3000, n0(50,1));
}

// int main() {
//   float r_work_power, r_currnet, r_eff, r_pf, r_torque, k_Tst_Tn, k_Ist_In,
//       k_Tmax_Tn;
//   int k_ne, k_noise, k_n0;
//   // scanf("%d%f%f%d%f%f%f%f%f%f%d",&k_n0, &r_work_power, &r_currnet, &k_ne,
//   // &r_eff, &r_pf,
//   //       &r_torque, &k_Tst_Tn, &k_Ist_In, &k_Tmax_Tn,&k_noise);

//   k_n0 = 3000;
//   r_work_power = 0.18;
//   r_currnet = 0.52;
//   k_ne = 2720;
//   r_eff = 65.9;
//   r_pf = 0.8;
//   r_torque = 0.63;
//   k_Tst_Tn = 2.3;
//   k_Ist_In = 5.5;
//   k_Tmax_Tn = 2.2;
//   k_noise = 61;

  

//   return 0;
// }

