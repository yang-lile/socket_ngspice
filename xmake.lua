---@diagnostic disable: undefined-global
add_rules("mode.debug", "mode.release")

-- ngspice test
-- ngspice import
if is_plat("windows") then
    add_requires("vcpkg::ngspice",
                 {alias = "ngspice", configs = {shared = true}})
else
    add_repositories("self-repo self_repo")
    add_requires("ngspice 3.9.3", {configs = {shared = true}})
end

-- main ngspice using test
target("socket_ngspice")
do
    set_languages("c11", "cxx17")
    set_kind("binary")
    add_packages("ngspice")
    add_includedirs("src/include")
    add_files("test/ngspice_test.c")
end
target_end()

target("count_parameter")
do
    set_kind("static")
    add_includedirs("src/include")
    add_files("src/tools/count_parameter.cc")
end
target_end()

-- 总测试代码 
add_requires("gtest")

-- 优化的添加测试文件函数
target("init_machine_test")
do
    set_kind("static")
    add_includedirs("src/include")
    add_deps("count_parameter")
    add_packages("gtest")
    add_files("test/count_parameter_test.cc")
end
target_end()

-- 测试头
target("test")
do
    set_kind("binary")
    add_packages("gtest")
    add_includedirs("src/include")
    add_deps("init_machine_test")
    add_files("test/main.cc")
end
target_end()

-- 以下是 protobuf 的测试
add_requires("protobuf-cpp")
-- add_requires("grpc")

target("grpc_server")
do
    set_kind("binary")
    add_packages("protobuf-cpp", {public = true})
    -- add_packages("grpc")
    add_rules("protobuf.cpp")
    add_files( -- "proto_gen/*.pd.cc",
    -- "proto_gen/helloworld.pb.cc",
    "test/grpc_server_test.cpp")
    add_files("src/proto/*.proto", {proto_public = true})
end
target_end()

add_requires("libcurl")

target("test_curl")
set_kind("binary")
add_packages("libcurl")
add_files("test/curl_test.c")
target_end()

-- tcp test
add_requires("libhv")

-- impl by cpp
target("client_cpp")
set_kind("binary")
add_packages("libhv")
add_files("test/tcp_client_test.cpp")
target_end()

target("server_cpp")
set_kind("binary")
add_packages("libhv")
add_files("test/tcp_server_test.cpp")
target_end()

-- impl by c
target("client_c")
set_kind("binary")
add_packages("libhv")
add_files("test/tcp_client_test.c")
target_end()

target("server_c")
set_kind("binary")
add_packages("libhv")
add_files("test/tcp_server_test.c")
target_end()

