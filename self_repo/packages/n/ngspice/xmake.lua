package("ngspice")
do
    add_urls("https://git.code.sf.net/p/ngspice/ngspice.git")

    add_versions("3.9.3", "f73d3b20a07cd206db8d7c7dd8679e6eecb02391", { verify = false })

    on_install("macosx", "linux", "mingw", function(package)
        if is_plat("macosx", "linux") then
            import("package.tools.autoconf").install(package, {
                "--with-ngshared",
                "--enable-xspice",
                "--enable-cider",
                "--enable-openmp",
                "--disable-debug",
                "--enable-osdi",
                "--prefix=/usr",
                "CFLAGS=-m64 -O2",
                "LDFLAGS=-m64 -s"
            })
        elseif is_plat("mingw") then
            import("package.tools.autoconf").install(package, {
                "--with-ngshared",
                "--enable-xspice",
                "--enable-cider",
                "--enable-openmp",
                "--disable-debug",
                "--enable-osdi",
                "--enable-relpath",
                "--prefix=C:/Spice64",
                [[CFLAGS=-m64 -O2]],
                [[LDFLAGS=-m64 -s]]
            })
        end
    end)
end
