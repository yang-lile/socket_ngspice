#if defined(__MINGW32__)
#include <winbase.h> /* Sleep */
#elif defined(_MSC_VER)
#include <windows.h> /* Sleep */
#else
#include <unistd.h> /* usleep */
#endif

inline void sleep_ms(int milliseconds) {
#if defined(__MINGW32__) || defined(_MSC_VER)
  Sleep(milliseconds); /* va: windows native */
#else
  usleep(milliseconds * 1000);
#endif
}
